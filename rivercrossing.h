//////////////////////////////////
// file:     rivercrossing.h
// project:  IOS - project 2
// date:     2014-05-04
//
// author:  Pavel Mencner
//          University of technology Brno - FIT
//
//////////////////////////////////

#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <unistd.h>
#include <semaphore.h>
#include <signal.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <sys/mman.h>
#include <sys/types.h>
#include <sys/wait.h>

#define EXPECTED_ARGC 5

enum error {E_OK = 0, E_PARAMS = 1, E_SYSCALL = 2};
enum person {HACKER, SERF};
enum action {START, WAIT, BOARD, MEMBER, CAPTAIN, LAND, FINISH};

typedef struct Params {
  unsigned P; //Persons in each category
  unsigned H; //max time (ms) between Hacker Gen.
  unsigned S; //max time (ms) between Serf Gen.
  unsigned R; //max time (ms) of Rivercrossing
  FILE *f;
}Params;

typedef struct SharedMem {
  unsigned long actionCounter; //use semFile
  unsigned doneCounter; //count persons who finish its sailing
  unsigned waitingSerfs; //number of processes waiting for boarding
  unsigned waitingHackers;
  unsigned hackerId; //local proces identifer (seved biggest one)
  unsigned serfId;
  unsigned membersReady; //num of members ready to go (and print member)
  FILE *f; //use semFile
  sem_t semFile; //access writing to File (and actionCounter)
  sem_t semPier; //access to Pier
  sem_t semWaitHackers; //control a boarding of Hackers
  sem_t semWaitSerfs; //control a boarding of Serfs
  sem_t semWaitForCrew; //captain wait for his crew
  sem_t semBoatEntry; //opens boat for boarding
  sem_t semSailing; //crew waits for sailing end
  sem_t semFinish; //where all processes waits after boarding
  sem_t semCounters; //control writting to all unsigned vars in ShMem
}SharedMem;

int convertToUnsigned(char *s, unsigned *storage);
int getParams(int argc, char **argv, Params *p);
unsigned randNum(unsigned roof);
void printState(SharedMem *shMem, int person, unsigned id, int action);
int genHackers(Params *p, SharedMem *shMem);
int genSerfs(Params *p, SharedMem *shMem);
void hacker(Params *p, SharedMem *shMem);
void serf(Params *p, SharedMem *shMem);
void hackerCaptain(Params *p, SharedMem *shMem, unsigned localId);
void serfCaptain(Params *p, SharedMem *shMem, unsigned localId);
void hackerMember (SharedMem *shMem, unsigned localId);
void serfMember (SharedMem *shMem, unsigned localId);
void iAmReady(SharedMem *shMem);
int loadSources(int *shmid, SharedMem **shMem);
int freeSources(int *shmid, SharedMem *shMem);
