//////////////////////////////////
// file:     hackerserf.c
// project:  IOS - project 2
// date:     2014-05-04
//
// author:  Pavel Mencner
//          University of technology Brno - FIT
//
//////////////////////////////////

#include "rivercrossing.h"

#define WS (shMem->waitingSerfs)
#define WH (shMem->waitingHackers)

int genHackers(Params *p, SharedMem *shMem) {
  srand(time(NULL)); //non pseudo number randomization
  pid_t *pid = (pid_t *)malloc(sizeof(pid_t) * p->P);
  if(pid == NULL) {return 1;}
  
  for(unsigned i = 0; i < p->P; i++) {
    usleep(randNum(p->H) * 1000); //usleep need micro seconds
    
    pid[i] = fork();
    if(pid[i] == 0) {
      free(pid);
      hacker(p, shMem); //child process
    }
    else if(pid[i] == -1) {
      fprintf(stderr, "Error, system call failure in fork()\n");
    }
  }
  
  int status;
  for(unsigned i = 0; i < p->P; i++) {
    waitpid(pid[i], &status, 0); //waiting for children
  }
  
  free(pid);
  exit(0);
}

int genSerfs(Params *p, SharedMem *shMem) {
  srand(time(NULL) + 14); //i asked a girlfriend for random number and
                          //she told 14, so its random seed to improve
                          //gen. in 2 same time running processes
  pid_t *pid = (pid_t *)malloc(sizeof(pid_t) * p->P);
  if(pid == NULL) {return 1;}
  
  for(unsigned i = 0; i < p->P; i++) {
    usleep(randNum(p->S) * 1000); //usleep need micro seconds
    
    pid[i] = fork();
    if(pid[i] == 0) {
      free(pid);
      serf(p, shMem); //child process
    }
    else if(pid[i] == -1) {
      fprintf(stderr, "Error, system call failure in fork()\n");
    }
  }
  
  int status;
  for(unsigned i = 0; i < p->P; i++) {
    waitpid(pid[i], &status, 0); //waiting for children
  }
  
  free(pid);
  exit(0);
}

void hacker(Params *p, SharedMem *shMem) {
  sem_wait(&shMem->semCounters);
    unsigned localId = ++(shMem->hackerId);
  sem_post(&shMem->semCounters);
  
  printState(shMem, HACKER, localId, START);
  
  sem_wait(&shMem->semPier); //try (wait) to go to pier
    sem_wait(&shMem->semCounters);
      shMem->waitingHackers++; //secure increment
    sem_post(&shMem->semCounters);
    
    printState(shMem, HACKER, localId, WAIT);

    sem_wait(&shMem->semCounters); //if() works with counters
    if(((WH - (WH % 2)) + (WS - (WS % 2))) < 4) {
      sem_post(&shMem->semCounters);
      sem_post(&shMem->semPier); //we dont have group, let next person come
    }
    else {
      //////////iam a Hacker captain of a boat//////////
      sem_post(&shMem->semCounters);
      hackerCaptain(p, shMem, localId);
    }
    //////////part of boarding member//////////
    hackerMember(shMem, localId);
    //this funtion never reach end
}

void serf(Params *p, SharedMem *shMem) {
  sem_wait(&shMem->semCounters);
    unsigned localId = ++(shMem->serfId);
  sem_post(&shMem->semCounters);
  
  printState(shMem, SERF, localId, START);
  
  sem_wait(&shMem->semPier); //try (wait) to go to pier
    sem_wait(&shMem->semCounters);
      shMem->waitingSerfs++; //secure increment
    sem_post(&shMem->semCounters);
    
    printState(shMem, SERF, localId, WAIT);
    
    sem_wait(&shMem->semCounters); //if() works with counters
    if(((WH - (WH % 2)) + (WS - (WS % 2))) < 4) {
      sem_post(&shMem->semCounters);
      sem_post(&shMem->semPier); //we dont have group, let next person come
    }
    else {
      //////////iam a Serf captain of a boat//////////
      sem_post(&shMem->semCounters);
      serfCaptain(p, shMem, localId);
    }
    //////////part of boarding member//////////
    serfMember(shMem, localId);
    //this funtion never reach end
}

void hackerCaptain(Params *p, SharedMem *shMem, unsigned localId) {
  sem_wait(&shMem->semCounters);
  if(WH == 4) { //all crew only hackers
    shMem->waitingHackers -= 4;
    sem_post(&shMem->semCounters);
    
    sem_post(&shMem->semWaitHackers);
    sem_post(&shMem->semWaitHackers);
    sem_post(&shMem->semWaitHackers);
  }
  else { //2H and 2S crew
    shMem->waitingHackers -= 2;
    shMem->waitingSerfs -= 2;
    sem_post(&shMem->semCounters);
    
    sem_post(&shMem->semWaitHackers);
    sem_post(&shMem->semWaitSerfs);
    sem_post(&shMem->semWaitSerfs);
  }
  
  sem_wait(&shMem->semWaitForCrew); //wait for your crew is ready
  //now we have crew to board waiting on semBoatEntry
  printState(shMem, HACKER, localId, BOARD);
  
  sem_post(&shMem->semBoatEntry);
  sem_post(&shMem->semBoatEntry);
  sem_post(&shMem->semBoatEntry);
  
  sem_wait(&shMem->semWaitForCrew);
  //now we have crew to board waiting on semSailing
  printState(shMem, HACKER, localId, CAPTAIN);
  usleep(randNum(p->R) * 1000);
  
  sem_post(&shMem->semSailing); //sailing ends, unloack members
  sem_post(&shMem->semSailing);
  sem_post(&shMem->semSailing);
  
  sem_wait(&shMem->semWaitForCrew);
  printState(shMem, HACKER, localId, LAND);
  
  sem_wait(&shMem->semCounters);
    shMem->doneCounter += 4;
  //wait for last sailing to end
  if(shMem->doneCounter == p->P * 2) {
    sem_post(&shMem->semCounters);
    sem_post(&shMem->semFinish); //iam last captain, start destruction
  }
  else {
    sem_post(&shMem->semCounters);
    sem_post(&shMem->semPier); //unlock pier for waiting processes
    
    sem_wait(&shMem->semFinish); //wait to last
    sem_post(&shMem->semFinish);
  }
  
  printState(shMem, HACKER, localId, FINISH);
  
  exit(0);
}

void hackerMember (SharedMem *shMem, unsigned localId) {
  sem_wait(&shMem->semWaitHackers); //waiting on pier
  
  printState(shMem, HACKER, localId, BOARD);
  iAmReady(shMem);
  
  sem_wait(&shMem->semBoatEntry); //wait for boarding start
    
  printState(shMem, HACKER, localId, MEMBER);
  iAmReady(shMem);
  
  sem_wait(&shMem->semSailing); //wait for sailing end
      
  printState(shMem, HACKER, localId, LAND);
  iAmReady(shMem);
  
  sem_wait(&shMem->semFinish); //wait for last sailing ends
  sem_post(&shMem->semFinish); //ech proc. unleash next waiting process
  
  printState(shMem, HACKER, localId, FINISH);

  exit(0);
}

void serfCaptain(Params *p, SharedMem *shMem, unsigned localId) {
  sem_wait(&shMem->semCounters);
  if(WS == 4) { //all crew only serfs
    shMem->waitingSerfs -= 4;
    sem_post(&shMem->semCounters);
    
    sem_post(&shMem->semWaitSerfs);
    sem_post(&shMem->semWaitSerfs);
    sem_post(&shMem->semWaitSerfs);
  }
  else { //2H and 2S crew
    shMem->waitingHackers -= 2;
    shMem->waitingSerfs -= 2;
    sem_post(&shMem->semCounters);
    
    sem_post(&shMem->semWaitSerfs);
    sem_post(&shMem->semWaitHackers);
    sem_post(&shMem->semWaitHackers);
  }
  
  sem_wait(&shMem->semWaitForCrew); //wait for your crew is ready
  //now we have crew to board waiting on semBoatEntry
  printState(shMem, SERF, localId, BOARD);
  
  sem_post(&shMem->semBoatEntry);
  sem_post(&shMem->semBoatEntry);
  sem_post(&shMem->semBoatEntry);
  
  sem_wait(&shMem->semWaitForCrew);
  //now we have crew to board waiting on semSailing
  printState(shMem, SERF, localId, CAPTAIN);
  usleep(randNum(p->R) * 1000);
  
  sem_post(&shMem->semSailing); //sailing ends, unloack members
  sem_post(&shMem->semSailing);
  sem_post(&shMem->semSailing);
  
  sem_wait(&shMem->semWaitForCrew);
  printState(shMem, SERF, localId, LAND);
  
  sem_wait(&shMem->semCounters);
    shMem->doneCounter += 4;
  //wait for last sailing to end
  if(shMem->doneCounter == p->P * 2) {
    sem_post(&shMem->semCounters);
    sem_post(&shMem->semFinish); //iam last captain, start destruction
  }
  else {
    sem_post(&shMem->semCounters);
    sem_post(&shMem->semPier); //unlock pier for waiting processes
    
    sem_wait(&shMem->semFinish); //wait for last sailing ends
    sem_post(&shMem->semFinish);
  }
  
  printState(shMem, SERF, localId, FINISH);
  
  exit(0);
}

void serfMember (SharedMem *shMem, unsigned localId) {
  sem_wait(&shMem->semWaitSerfs); //waiting on pier
  
  printState(shMem, SERF, localId, BOARD);
  iAmReady(shMem);
  
  sem_wait(&shMem->semBoatEntry); //wait for boarding start
    
  printState(shMem, SERF, localId, MEMBER);
  iAmReady(shMem);
  
  sem_wait(&shMem->semSailing); //wait for board end
      
  printState(shMem, SERF, localId, LAND);
  iAmReady(shMem);
  
  sem_wait(&shMem->semFinish); //wait for last boarding ends
  sem_post(&shMem->semFinish); //ech proc. unleash next waiting process
  
  printState(shMem, SERF, localId, FINISH);

  exit(0);
}
