##################################
## file:     Makefile
## project:  IOS - project 2
## date:     2014-05-04
##
## author:  Pavel Mencner
##          University of technology Brno - FIT
## 
##################################

rivercrossing: rivercrossing.o hackerserf.o
	gcc rivercrossing.o hackerserf.o -pthread -o rivercrossing

rivercrossing.o: rivercrossing.c rivercrossing.h
	gcc rivercrossing.c -std=gnu99 -Wall -Wextra -Werror -pedantic -pthread -c

hackerserf.o: hackerserf.c rivercrossing.h
	gcc hackerserf.c -std=gnu99 -Wall -Wextra -Werror -pedantic -pthread -c
