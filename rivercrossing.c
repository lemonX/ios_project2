//////////////////////////////////
// file:     rivercrossing.c
// project:  IOS - project 2
// date:     2014-05-04
//
// author:  Pavel Mencner
//          University of technology Brno - FIT
//
//////////////////////////////////

#include "rivercrossing.h"

/**
 * Parse params from command line
 * @param argc number of params
 * @param argv array of string with params
 * @param p pointer to struct where save params
 * @return 0 if OK || !=0 if error
 */
int getParams(int argc, char **argv, Params *p) {
  
  if (argc != EXPECTED_ARGC) {
    return E_PARAMS;
  }
  
  if ((convertToUnsigned(argv[1], &p->P) != 0) || (p->P == 0) || (p->P % 2 != 0)) {
    return E_PARAMS;
  }
  if ((convertToUnsigned(argv[2], &p->H) != 0) || (p->H >= 5001)) {
    return E_PARAMS;
  }
  if ((convertToUnsigned(argv[3], &p->S) != 0) || (p->S >= 5001)) {
    return E_PARAMS;
  }
  if ((convertToUnsigned(argv[4], &p->R) != 0) || (p->R >= 5001)) {
    return E_PARAMS;
  }
  
  if((p->f = fopen("rivercrossing.out","w")) == NULL) {
    return E_SYSCALL;
  }
  setbuf(p->f, NULL); //turn the file buffer of
  
  return E_OK;
}

/**
 * Convert string to unsigned int value
 * @param s pointer to string to convert
 * @param storage pointer to destination var.
 * @return 1 if unexpected chars
 */
int convertToUnsigned(char *s, unsigned *storage) {
  char *foo = NULL;  
  *storage = (unsigned)strtoul(s, &foo, 10);
  if(foo == s || *foo != '\0') {
    return 1;
  }
  return 0;
}

/**
 * Randomize number <0; roof>
 * @param roof max number
 * @return randomize number
 */
unsigned randNum(unsigned roof) {
  return (roof > 0)? ((unsigned) (rand() % (roof + 1))): 0;
}

/**
 * Count members which are ready, so captain know when start some action
 * Last member clear shared counter and free a captain on semaphore
 * @param shMem pointer to shared memmory
 */
void iAmReady(SharedMem *shMem) {
  sem_wait(&shMem->semCounters);
  if(++shMem->membersReady == 3) {
    shMem->membersReady = 0;
    sem_post(&shMem->semWaitForCrew);
  }
  sem_post(&shMem->semCounters);
}

/**
 * Print process states secure (use semaphores)
 * @param shMem pointer to shared memmory
 * @param person (enum) HACKER || SERF
 * @param id internal person identifer
 * @param action (enum) printed action
 */
void printState(SharedMem *shMem, int person, unsigned id, int action) {
  sem_wait(&shMem->semFile);
    fprintf(shMem->f, "%lu: %s: %u: ", ++(shMem->actionCounter), (person == HACKER)? "hacker": "serf", id);
    
    if(action == START) {
      fprintf(shMem->f, "started\n");
    }
    else if(action == WAIT) {
      sem_wait(&shMem->semCounters);
        fprintf(shMem->f, "waiting for boarding: %u: %u\n", shMem->waitingHackers, shMem->waitingSerfs);
      sem_post(&shMem->semCounters);
    }
    else if(action == BOARD) {
      sem_wait(&shMem->semCounters);
        fprintf(shMem->f, "boarding: %u: %u\n", shMem->waitingHackers, shMem->waitingSerfs);
      sem_post(&shMem->semCounters);
    }
    else if(action == MEMBER) {
      fprintf(shMem->f, "member\n");
    }
    else if(action == CAPTAIN) {
      fprintf(shMem->f, "captain\n");
    }
    else if(action == LAND) {
      sem_wait(&shMem->semCounters);
        fprintf(shMem->f, "landing: %u: %u\n", shMem->waitingHackers, shMem->waitingSerfs);
      sem_post(&shMem->semCounters);
    }
    else if(action == FINISH) {
      fprintf(shMem->f, "finished\n");
    }
    
  sem_post(&shMem->semFile);
}

/**
 * Get attach and init shared memmory and its variables and semaphores
 * @param shmid pointer where we save shmget() return 
 * @param shMem pointer to pointer where we save shmat() return
 * @return E_OK or E_SYSCALL
 */
int loadSources(int *shmid, SharedMem **shMem) {
  //get shared memmory for Shared Sems and Vars
  *shmid = shmget(IPC_PRIVATE, sizeof(SharedMem), IPC_CREAT | 0666);
  if(*shmid == -1) {
    return E_SYSCALL;
  }
  //attach shared memmory to memmory space
  if((*shMem = (SharedMem *)shmat(*shmid, NULL, 0)) == NULL) {
    freeSources(shmid, NULL);
    return E_SYSCALL;
  }
  //Semaphores init
  int ecode = E_OK;
  if(sem_init(&((*shMem)->semFile), 1, 1) == -1) {ecode = E_SYSCALL;}
  if(sem_init(&((*shMem)->semPier), 1, 1) == -1) {ecode = E_SYSCALL;}
  if(sem_init(&((*shMem)->semWaitHackers), 1, 0) == -1) {ecode = E_SYSCALL;}
  if(sem_init(&((*shMem)->semWaitSerfs), 1, 0) == -1) {ecode = E_SYSCALL;}
  if(sem_init(&((*shMem)->semWaitForCrew), 1, 0) == -1) {ecode = E_SYSCALL;}
  if(sem_init(&((*shMem)->semBoatEntry), 1, 0) == -1) {ecode = E_SYSCALL;}
  if(sem_init(&((*shMem)->semSailing), 1, 0) == -1) {ecode = E_SYSCALL;}
  if(sem_init(&((*shMem)->semFinish), 1, 0) == -1) {ecode = E_SYSCALL;}
  if(sem_init(&((*shMem)->semCounters), 1, 1) == -1) {ecode = E_SYSCALL;}
  
  if(ecode == E_SYSCALL) {
    freeSources(shmid, *shMem);
    return E_SYSCALL;
  }
  //Data init
  (*shMem)->actionCounter = 0;
  (*shMem)->waitingHackers = 0;
  (*shMem)->waitingSerfs = 0;
  (*shMem)->hackerId = 0;
  (*shMem)->serfId = 0;
  (*shMem)->membersReady = 0;
  (*shMem)->doneCounter = 0;
  return E_OK;
}

/**
 * destroy semaphores, detach and clear shared memmory
 * @param shmid pointer to allocated memmory from shmget()
 * @param shMem pointer to attached memmory from shmat()
 * @return E_OK or E_SYSCALL
 */
int freeSources(int *shmid, SharedMem *shMem) {
  int ecode = E_OK;
  
  if(shMem != NULL) {
    if(shMem->f != NULL) {
      fclose(shMem->f); //close output file
      shMem->f = NULL;
    }  
    //destroy semaphores
    if(sem_destroy(&(shMem->semFile)) == -1) {ecode = E_SYSCALL;}
    if(sem_destroy(&(shMem->semPier)) == -1) {ecode = E_SYSCALL;}
    if(sem_destroy(&(shMem->semWaitHackers)) == -1) {ecode = E_SYSCALL;}
    if(sem_destroy(&(shMem->semWaitSerfs)) == -1) {ecode = E_SYSCALL;}
    if(sem_destroy(&(shMem->semWaitForCrew)) == -1) {ecode = E_SYSCALL;}
    if(sem_destroy(&(shMem->semBoatEntry)) == -1) {ecode = E_SYSCALL;}
    if(sem_destroy(&(shMem->semSailing)) == -1) {ecode = E_SYSCALL;}
    if(sem_destroy(&(shMem->semFinish)) == -1) {ecode = E_SYSCALL;}
    if(sem_destroy(&(shMem->semCounters)) == -1) {ecode = E_SYSCALL;}
  
    if(shmdt(shMem) == -1) {ecode = E_SYSCALL;} //detach shared memmory
  }
  
  if(shmid != NULL) { //free shared memmory
    if(shmctl(*shmid, IPC_RMID, NULL) == -1) { ecode = E_SYSCALL;}
  }
  
  return ecode;
}

int main(int argc, char **argv) {  
  
  srand(time(NULL)); //non pseudo number randomization
  Params params;
  SharedMem *shMem = NULL;
  int shmid = 0;
    
  pid_t hackerGenPid, serfGenPid;
  int eCode;
  
  if((eCode = getParams(argc, argv, &params)) != E_OK) {
    if(eCode == E_PARAMS) {
      fprintf(stderr, "Error, wrong params\n");
      return 1;
    }
    else {
      fprintf(stderr, "Error, cant open file rivercrossing.out\n");
      return 2;
    }
  }
  
  if(loadSources(&shmid, &shMem) != E_OK) {
    fprintf(stderr, "Error, system call failure in loadSources()\n");
    return 2;
  }
  shMem->f = params.f;
  
  hackerGenPid = fork(); 
  if (hackerGenPid == 0) {
    //forked process, Hackers generator
    genHackers(&params, shMem);
  }
  else if (hackerGenPid == -1) {
    fprintf(stderr, "Error, system call failure in fork()\n");
    freeSources(&shmid, shMem);
    return E_SYSCALL;
  }
  
  serfGenPid = fork(); 
  if (serfGenPid == 0) {
    //forked process, Serfs generator
    genSerfs(&params, shMem);
  }
  else if (serfGenPid == -1) {
    fprintf(stderr, "Error, system call failure in fork()\n");
    freeSources(&shmid, shMem);
    return E_SYSCALL;
  }

  int status;
  waitpid(hackerGenPid, &status, 0); //wait for children 
  waitpid(serfGenPid, &status, 0);
  
  if(freeSources(&shmid, shMem) != E_OK) {
    fprintf(stderr, "Error, system call failure during clening sources\n");
    return E_SYSCALL;
  }
  
  return 0;
}
